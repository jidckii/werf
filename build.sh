#!/usr/bin/env bash

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/jidckii/werf'}
CI_COMMIT_TAG=${CI_COMMIT_TAG:-'latest'}

build() {
	echo "==> Building Docker image..."
	docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} -f ./Dockerfile .
}

push() {
	echo "==> Docker push..."
	docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
}

default() {
    build
    push
}

case ${1} in
build) build ;;
push) push ;;
* ) default ;;
esac
