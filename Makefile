.DEFAULT_GOAL := default

.PHONY: build
build:
	./build.sh build

.PHONY: push
push:
	./build.sh push

.PHONY: default
default: build
default: push
