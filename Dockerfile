FROM buildpack-deps:bullseye-curl as build
RUN set -xe \
    && curl -L "https://tuf.trdl.dev/targets/releases/0.1.3/linux-amd64/bin/trdl" -o /tmp/trdl \
    && install /tmp/trdl /usr/local/bin/trdl \
    && trdl add werf https://tuf.werf.io 1 b7ff6bcbe598e072a86d595a3621924c8612c7e6dc6a82e919abe89707d7e3f468e616b5635630680dd1e98fc362ae5051728406700e6274c5ed1ad92bea52a2 \
    && . $(trdl use werf 1.2 ea) \
    && find /root -name "werf" | grep bin | xargs -I{} cp -u {} /usr/local/bin

FROM docker:git as app
COPY --from=build /usr/local/bin/werf /usr/local/bin/werf
CMD [ "werf", "version" ]
